package hr.fer.ruazosa.lecture5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.runBlocking

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val myViewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(MyViewModel::class.java)

        myViewModel.resultOfDataFetch.observe(this, Observer {
            timeStatusTextView.text = it
        })

        if(StopwatchStatus.status.value == null) {
            StopwatchStatus.status.value = "STOPPED"
            myViewModel.resultOfDataFetch.value = "0"

            setButtonsAfterStop()
        }

        startButton.setOnClickListener {
            StopwatchStatus.status.value = "STARTED"
            StopwatchStatus.coroutine.value = myViewModel.fetchDataFromRepository()

            setButtonsAfterStart()
        }

        pauseButton.setOnClickListener {
            StopwatchStatus.status.value = "PAUSED"
            StopwatchStatus.coroutine.value?.cancel()

            setButtonsAfterPause()
        }

        stopButton.setOnClickListener {
            StopwatchStatus.status.value = "STOPPED"
            StopwatchStatus.coroutine.value?.cancel()
            myViewModel.resultOfDataFetch.value = "0"


            setButtonsAfterStop()
        }
    }

    override fun onResume() {
        super.onResume()

        when(StopwatchStatus.status.value) {
            "STARTED" -> setButtonsAfterStart()
            "PAUSED" -> setButtonsAfterPause()
            "STOPPED" -> setButtonsAfterStop()
        }
    }

    private fun setButtonsAfterStart() {
        startButton.visibility = View.VISIBLE
        startButton.isClickable = false
        pauseButton.visibility = View.VISIBLE
        pauseButton.isClickable = true
        stopButton.visibility = View.VISIBLE
        stopButton.isClickable = true
    }

    private fun setButtonsAfterPause() {
        startButton.isClickable = true
        pauseButton.isClickable = false
        stopButton.isClickable = true
    }

    private fun setButtonsAfterStop() {
        startButton.visibility = View.VISIBLE
        startButton.isClickable = true
        pauseButton.visibility = View.INVISIBLE
        pauseButton.isClickable = false
        stopButton.visibility = View.INVISIBLE
        stopButton.isClickable = false
    }
}
