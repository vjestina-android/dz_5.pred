package hr.fer.ruazosa.lecture5

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Job

object StopwatchStatus {
    val status = MutableLiveData<String>()
    val coroutine = MutableLiveData<Job>()
}