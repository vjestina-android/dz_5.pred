package hr.fer.ruazosa.lecture5

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*

class MyViewModel: ViewModel() {

    val resultOfDataFetch = MutableLiveData<String>()

    fun fetchDataFromRepository(): Job {
            return viewModelScope.launch {
                withContext(Dispatchers.IO) {
                    var i = 0
                    if(resultOfDataFetch.value != null) i = resultOfDataFetch.value.toString().toInt()
                    while (isActive) {
                        i++;
                        val fetchedResult = MyRepository.fetchData(i)
                        withContext(Dispatchers.Main) {
                                resultOfDataFetch.value = fetchedResult
                        }
                    }
                }
            }
    }

}